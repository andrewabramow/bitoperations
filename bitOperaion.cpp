/*
* Дан вектор из восьми uint8 чисел.
* Составить число из  младших битов этих чисел.
*/

#include <iostream>
#include <vector>
#include <bitset>

int main()
{
    std::vector<uint8_t> n{ 3,1,1,1,1,1,1,1 };
    uint8_t res = 0;
    uint8_t lowBitMask = 1;

    for (int i = 0; i < n.size(); ++i)
    {
        std::cout << "vec el: " << std::bitset<8>(n[i]) << std::endl;

        auto iLowBit = n[i] & lowBitMask;
        std::cout << "low bit of i element: " << std::bitset<8>(iLowBit) << std::endl;

        res |= (iLowBit <<i);
        std::cout << "current result: " << std::bitset<8>(res) << std::endl << std::endl;
    }
}
